from collections import deque, namedtuple
from math import inf

Edge = namedtuple('Edge', ['start', 'end', 'cost'])


class Graph:
    def __init__(self, *args):
        self.edges = [Edge(item[0], item[1], int(item[2])) for item in args]

    @property
    def vertices(self):
        return set(
            # this piece of magic turns ([1, 2], [3, 4]) into [1, 2, 3, 4]
            # the set above makes it's elements unique.
            sum(
                ([edge.start, edge.end] for edge in self.edges), []
            )
        )

    def remove_edge(self, n1, n2):
        node_pairs = [[n1, n2]]
        edges = self.edges[:]
        for edge in edges:
            if [edge.start, edge.end] in node_pairs:
                self.edges.remove(edge)
    
    def add_edge(self, n1, n2, cost=1):
        node_pairs = [[n1, n2]]
        for edge in edges:
            if [edge.start, edge.end] in node_pairs:
                return ValueError(f'Edge {n1} {n2} already exists')
        self.edges.append(Edge(start=n1, end=n2, cost=cost))
    
    @property
    def neighbours(self):
        neighbours = {vertex: set() for vertex in self.vertices}
        for edge in self.edges:
            neighbours[edge.start].add((edge.end, edge.cost))

        return neighbours

    def dijkstra(self, source, dest):
        assert source in self.vertices, 'Such source node does not exist'

        # 1. Mark all nodes unvisited and store them
        # 2. Set the distance to zero for our initial node
        #    and to infinity for other nodes
        distances = {vertex: inf for vertex in self.vertices}
        previous_vertices = {
            vertex: None for vertex in self.vertices
        }
        distances[source] = 0
        vertices = self.vertices.copy()
        while vertices:
            # 3. Select the unvisited node with the smallest distance,
            #    it's current node now.
            current_vertex = min(vertices, key=lambda vertex: distances[vertex])
            # 6. Stop, if the smallest distance
            #    among the unvisited nodes is infinity.
            if distances[current_vertex] == inf:
                break

            # 4. Find unvisited neighbours for the current node
            #    and calculate their distances through the current node.
            for neighbour, cost in self.neighbours[current_vertex]:
                alternative_route = distances[current_vertex] + cost

                # Compare the newly calculated distance to the assigned
                # and save the smaller one.
                if alternative_route < distances[neighbour]:
                    distances[neighbour] = alternative_route
                    previous_vertices[neighbour] = current_vertex

            # 5. Mark the current node as visited
            #    and remove it from the unvisited set.
            vertices.remove(current_vertex)

        path, current_vertex = deque(), dest
        while previous_vertices[current_vertex] is not None:
            path.appendleft(current_vertex)
            current_vertex = previous_vertices[current_vertex]
        if path:
            path.appendleft(current_vertex)
        return path

    def compute_costs(self):
        return {(edge.start + edge.end): edge.cost for edge in self.edges}

    def distance(self, *args):
        """"""
        matrix = self.compute_costs()
        cost = 0
        for index, node in enumerate(args, 1):
            if index == len(args):
                break
            key = node + args[index]
            current = matrix.get(key)
            if not current:
                return 'NO SUCH ROUTE'
            cost += current
        return cost
