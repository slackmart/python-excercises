from graph import Graph


if __name__ == '__main__':
    graph = Graph('AB5', 'BC4', 'CD8', 'DC8', 'DE6', 'AD5', 'CE2', 'EB3', 'AE7')
    print("Output #1", graph.distance('A', 'B', 'C'))
    print("Output #2", graph.distance('A', 'D'))
    print("Output #3", graph.distance('A', 'D', 'C'))
    print("Output #4", graph.distance('A', 'E', 'B', 'C', 'D'))
    print("Output #5", graph.distance('A', 'E', 'D'))

    print('Output #8', graph.distance(*graph.dijkstra('A', 'C')))
    print('Output #9', graph.distance(*graph.dijkstra('B', 'B')))
