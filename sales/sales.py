import decimal
import math
import re


def wrap_and_clean(data):
    li = [item.strip() for item in data.split('\n') if item]
    lo = [item.split(' at ') for item in li]
    return map(lambda itm: [itm[0], float(itm[1])], lo)


def solve(data):
    cleaned_data = wrap_and_clean(data)

    pattern = re.compile(r'(pills|chocolate|book)')
    decimal.getcontext().rounding = decimal.ROUND_DOWN
    total_tax, amounts = 0, 0
    summary = []

    for item in cleaned_data:
        basic_tax, duty_tax = 0, 0
        concept, amount = item
        matches = pattern.search(concept)
        if not matches:
            basic_tax = 0.1 * amount

        if 'imported' in concept:
            duty_tax = 0.05 * amount

        tax = basic_tax + duty_tax
        amount += tax
        total_tax += tax
        amounts += amount
        summary.append(f'{concept} : {amount} => {round(amount, 2)}')

    return summary, total_tax, amounts


if __name__ == '__main__':
    first_input = """
    1 book at 12.49
    1 music CD at 14.99
    1 chocolate bar at 0.85"""

    second_input = """
    1 imported box of chocolates at 10.00
    1 imported bottle of perfume at 47.50"""

    third_input = """
    1 imported bottle of perfume at 27.99
    1 bottle of perfume at 18.99
    1 packet of headache pills at 9.75
    1 box of imported chocolates at 11.25"""

    for index, input_data in enumerate((first_input, second_input, third_input), 1):
        print(f'\nOutput #{index}')
        summary, total_tax, total_amount = solve(input_data)
        for line in summary:
            print(line)

        print('Sales Taxes:', total_tax)
        print('Total:', total_amount)
